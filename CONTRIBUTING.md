# Contributing

If you like to contribute to MIDAS, follow this workflow:

* Create or select an issue and assign yourself as Assignee.
  * If you create a new issue, write down what you want to do
* Click on "Create merge request" and make sure to select development as source
  branch
* Work on the issue
* Write a unit test
* Once you think you're finished and the pipeline succeeds, mark the merge
  request as ready

# Contributors

The first ideas for midas can be traced back to Martin Tröschel and Eric Veith. 
The first minimal prototype for midas was developed by Martin Tröschel.

## Version 1

The first version of midas (0.1.0) was developed in several teams:

* Powergrid (Sanja Stark, Frauke Oest, Stephan Balduin, Martin 
  Tröschel)
* Unit Models (Johannes Gerster, Rami Elshinawy, Stephan Balduin,
  Jörg Bremer)
* Agents (Stefanie Holly, Marvin Nebel-Wenner, Sanja Stark,
  Emilie Frost, Martin Tröschel, Jörg Bremer)
* ICT (Stefanie Holly, Frauke Oest, Thomas Raub)

## Version 2

* The blackstart/mango module was contributed by Sanja Stark
* The market agents were developed by Torge Wolff
* The qmarket was developed by Thomas Wolgast

Contributors of the second version of midas (>0.1.0, by order 
of appearance):

- Stephan Balduin <stephan.balduin@offis.de>
- Sanja Stark <sanja.stark@offis.de>
- Torge Wolff <torge.wolff@offis.de>
- Thomas Wolgast <thomas.wolgast@offis.de>
- Nils Wenninghoff <nils.wenninghoff@offis.de>
- Torben Woltjen <@mozzbozz>
- Ben Ostendorf <@benostendorf>

## Version 3

This version includes everything > 1.0.0. 
Since midas is now a namespace package, several packages contribute to the software stack.
Official packages at the start of this version are

- midas-mosaik (this, the parent/meta package)
- midas-store
- midas-timesim
- midas-powergrid
- midas-comdata
- midas-dlpdata
- midas-pwdata
- midas-sbdata
- midas-sndata
- midas-weather
- midas-goa
- midas-util
- midas-palaestrai
- (pysimmods)

Modules for market agents, qmarket, and mango will probably added in the near future.

New contributors with the third version of midas (by order of appearance):

- Paul Hendrik Tiemann <@pht.uol>